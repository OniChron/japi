package com.apibuilder.japi;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class JAPIController {

    @FXML
    private TextFlow leftTextFlow;
    @FXML
    private TextFlow rightTextFlow;


    //---- Start of Methods -----------------
    // For menu items //

    // -- Controller to add this FXML is not working, it needs to be corrected -> Output in the IDE "Login" not actually executing the loader -- //
    @FXML
    public void login(Stage stage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(JAPIApplication.class.getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 550, 500);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void quit(Event e) {
        Platform.exit();
    }
    // Login Method //
    @FXML

    private void login() {
        System.out.println("Login");
    }
    // Configuration Methods //

    // API List Actions //

}