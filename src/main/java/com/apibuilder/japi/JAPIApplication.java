package com.apibuilder.japi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class JAPIApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(JAPIApplication.class.getResource("JAPI.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1800, 800);
        stage.setTitle("JAPI");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}