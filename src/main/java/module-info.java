module com.apibuilder.japi {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;

    opens com.apibuilder.japi to javafx.fxml;
    exports com.apibuilder.japi;
}